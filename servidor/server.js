// server.js
// where your node app starts

// we've started you off with Express (https://expressjs.com/)
// but feel free to use whatever libraries or frameworks you'd like through `package.json`.
const express = require("express");
const app = express();
const axios = require("axios");

var bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var buscacep = require("busca-cep");
var mysql = require("mysql");
var moment = require("moment");

// our default array of dreams
const dreams = [
  "Find and count some sheep",
  "Climb a really tall mountain",
  "Wash the dishes"
];

// make all the files in 'public' available
// https://expressjs.com/en/starter/static-files.html
app.use(express.static("public"));

// https://expressjs.com/en/starter/basic-routing.html
app.get("/", (request, response) => {
  response.sendFile(__dirname + "/views/index.html");
});

// https://expressjs.com/en/starter/basic-routing.html
app.post("/thanos", (request, response) => {
  var intentName = request.body.queryResult.intent.displayName;

  if (intentName == "Cardapio") {
    const url =
      "https://sheet.best/api/sheets/d1e10436-0e63-4d83-9db9-00ab2f939ec5";

    axios
      .get(url)
      .then(res => {
        console.log(res.data);
      })
      .catch(err => console.log(err))
      .then(respon => {
        response.json({ fulfillmentText: "Primeiro WebHook33333333333333333" });
      });
  } else if (intentName == "Totaldevendaporbandeiradecartao") {
    let bandeira = request.body.queryResult.parameters["bandeira"];
    let hora = request.body.queryResult.parameters["time"];
    
  
    let data = request.body.queryResult.parameters["date"];
   

    

    console.log("bandeira", bandeira);
    console.log("data", data);


    console.log("chegou aqui bd conectado");

    response.json({ fulfillmentText: "Teste Mysql" });
  } else if (intentName == "transacoesestabelecimento") {

    console.log("Entrei no intent transacoesestabelecimento");

  // de teste  response.json({ fulfillmentText: "Teste Teste" });

  response.json({
    "fulfillmentMessages": [
            {
              "card": {
                "title": "Relatório de Transações Estabelecimento",
                "subtitle": "Ele sempre será o primeiro item do Resumo de Conciliação a ser visto, conforme imagem a seguir",
                "imageUri": "https://firebasestorage.googleapis.com/v0/b/thanos-sxxlhc.appspot.com/o/imagem%2FTransacoesEstabelecimentoGeral.png?alt=media&token=97a91e9e-5977-4c0d-8ebd-bfb7a349ae58",
                
              }
            },
            {
              "text" :{
                 "text": [
                    "Apos clicar no menu, pode utilizar filtro de datas e bandeira conforme imagem abaixo:"
                ]
               
              }
            },
            {
              "text" :{
                 "text": [
                    "Ele confere se todas as tuas vendas no TEF, foram aceitas como vendas aptas a serem pagas nas redes adquirentes"
                    
                ]
              }
           
            },
            {
              "text":{
                "text" : [
                  "Tem mais alguma dúvida sobre o relatório de transações?"
                ]
              }

            },


  
          ]
   });
   


  }/* else if (intentName == "transacoesestabelecimento - yes"){
    console.log('entrei nesse transacoesestabelecimento-yes');
    response.json({ fulfillmentText: "[NGROK] dentro do yes por fora do dialogflow" });
  } */
  else if (intentName == "detalhadotransacaoestabelecimento"){
    console.log('entre no detalhadotransacaoestabelecimento');
   // response.json({fulfillmentText: "[NGROK] fora do dialogflow dentro do detalha transacao estabelecimento"});
   
   response.json({
    "fulfillmentMessages": [
            {
              "card": {
                "title": "Acessando o relatório transações estabeelecimento",
                "subtitle": "Ele sempre será o primeiro item do Resumo de Conciliação a ser visto, conforme imagem a segui",
                "imageUri": "https://firebasestorage.googleapis.com/v0/b/thanos-sxxlhc.appspot.com/o/imagem%2FRelatorio%20TransacoesSeparado.png?alt=media&token=6ac6a874-c4ba-4913-885b-5324ccb4568d",
                
              }
            },
            {
              "text" :{
                 "text": [
                    "Ele também pode ser acessado no menu a esquerda do Resumo de Conciliação, em vendas"
                ]
              }
            },
            {
              "text" :{
                 "text": [
                    "Ele confere se todas as tuas vendas no TEF, foram aceitas como vendas aptas a serem pagas nas redes adquirentes"
                    
                ]
              }
           
            },
            {
              "text":{
                "text" : [
                  "Tem mais alguma dúvida sobre o relatório de transações?"
                ]
              }

            },


  
          ]
  });
} else if (){


  
}
});

// send the default array of dreams to the webpage
app.get("/dreams", (request, response) => {
  // express helps us take JS objects and send them as JSON
  response.json(dreams);
  response.json({ fulfillmentText: "Primeiro WebHook33333333333333333" });
});

// listen for requests :)
const listener = app.listen(3000, () => {
  console.log("Your app is listening on port " + listener.address().port);
});
